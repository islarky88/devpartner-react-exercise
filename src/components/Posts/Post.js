import React, { Component } from "react";

class Post extends Component {
 

  render() {
   
    return (
      <div class="post">
        <div class="postauthor">Written by: {this.props.id}</div>
        <div class="post-title">
          <b>{this.props.title}</b>
        </div>
        <div class="post-body">{this.props.body}</div>
      </div>
    );
  }
}



export default Post;
