import React, { Component } from "react";
import Post from "./Post";

class PostsContainer extends Component {
  renderPosts() {
    return this.props.postlist.map(post => (
      <Post id={post.id} title={post.title} body={post.body} />
    ));
  }

  render() {
    return <div class="postlist">{this.renderPosts()}</div>;
  }
}

export default PostsContainer;
