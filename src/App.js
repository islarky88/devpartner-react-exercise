import React, { Component } from "react";
import PostsContainer from "./components/Posts/PostsContainer";
import axios from "axios";


import "./styles.css";

//API: https://jsonplaceholder.typicode.com/posts

class App extends Component {
  state = {
    postlist: [],
    limit: 5
  };

  componentDidMount() {
    axios.get(`https://jsonplaceholder.typicode.com/posts?_limit=${this.state.limit}`).then(res => {
      const postlist = res.data;
      this.setState({ postlist });
    });
  }

  render() {
    return (
      <div className="App">
        
          <PostsContainer postlist={this.state.postlist} />
        
      </div>
    );
  }
}

export default App;
